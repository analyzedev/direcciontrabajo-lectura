from datetime import datetime
import requests
from pyquery import PyQuery as pq

from multas.models import Empresa, Multa

from itertools import cycle


def agregar_todos2():
    creadas = 0
    print(datetime.now())
    mayor = Empresa.objects.filter(incremental2=True).order_by('-ruti').first()
    if mayor is None:
        comienzo = 76000000
    else:
        comienzo = int(mayor.rut)

    delta = 3000
    print(datetime.now())
    existentes = [i["ruti"] for i in Empresa.objects.filter(ruti__gte=comienzo - 1, ruti__lte=comienzo + delta + 1).values('ruti')]
    for rut in range(comienzo, comienzo + delta):
        if rut > 80000000:
            return
        if rut not in existentes:
            empresa, created = Empresa.objects.get_or_create(rut=rut)
            if created:
                empresa.seleccion = True
                empresa.save()
                creadas += 1

    print(datetime.now())
    try:
        empresa, created = Empresa.objects.get_or_create(rut=rut)
    except:
        Empresa.objects.filter(rut=rut).order_by('id').last().delete()
        empresa, created = Empresa.objects.get_or_create(rut=rut)

    empresa.incremental2 = True
    empresa.save()
    print("creadas {} empresa comenzando desde {}".format(creadas, comienzo))


def agregar_todos():
    creadas = 0
    mayor = Empresa.objects.filter(incremental=True).order_by('-rut').first()
    if mayor is None:
        comienzo = 76000000
    else:
        comienzo = int(mayor.rut)
    print(datetime.now())

    for rut in range(comienzo, comienzo + 500):
        if rut > 79000000:
            return
        empresa, created = Empresa.objects.get_or_create(rut=rut)
        if created:
            empresa.seleccion = True
            empresa.save()
            creadas += 1

    print(datetime.now())
    empresa.incremental = True
    empresa.save()

    print("creadas {} empresa comenzando desde {}".format(creadas, comienzo))
    return

    import random
    for k in range(300):
        rut = random.randint(76000000, 79000000)
        empresa, created = Empresa.objects.get_or_create(rut=rut)
        if created:
            empresa.seleccion = True
            empresa.save()
            creadas += 1


def digito_verificador(rut):
    reversed_digits = map(int, reversed(str(rut)))
    factors = cycle(range(2, 8))
    s = sum(d * f for d, f in zip(reversed_digits, factors))
    return (-s) % 11


def read_by_empresa(empresa):
    data = {}
    if empresa.dv is None:
        empresa.dv = digito_verificador(empresa.rut)
        empresa.save()

    rut_completo = "{}-{}".format(empresa.rut, empresa.dv)
    url = 'http://ventanilla.dirtrab.cl/RegistroEmpleador/consultamultas.aspx'
    session = requests.session()
    page = pq(session.get(url).text)
    data["__VIEWSTATE"] = page.find("form>input[name='__VIEWSTATE']")[0].value
    data["__EVENTTARGET"] = "btnConsulta"
    data["__EVENTARGUMENT"] = page.find(
        "form>input[name='__EVENTARGUMENT']")[0].value
    data["__VIEWSTATEGENERATOR"] = page.find(
        "form>input[name='__VIEWSTATEGENERATOR']")[0].value
    data["tbxRut"] = rut_completo
    data["RadAjaxPanel1PostDataValue"] = "RadAjaxPanel1,ActiveElement,btnConsulta;"
    data["RadAJAXControlID"] = "RadAjaxPanel1"
    data["httprequest"] = "true"
    r = session.post(url, data=data)
    try:
        razonsocial = pq(r.text).find("#lblMensaje")[0].text_content().split("Razón social:")[1].split("multas encontradas")[0]
        if empresa.razonsocial is None:
            empresa.razonsocial = razonsocial
            empresa.save()
    except:
        razonsocial = None

    data = {}
    data["__EVENTTARGET"] = ""
    data["__VIEWSTATE"] = page.find("form>input[name='__VIEWSTATE']")[0].value
    data["__EVENTARGUMENT"] = page.find(
        "form>input[name='__EVENTARGUMENT']")[0].value
    data["__VIEWSTATEGENERATOR"] = page.find(
        "form>input[name='__VIEWSTATEGENERATOR']")[0].value
    data["tbxRut"] = rut_completo
    data["btnXls"] = "exportar a excel"
    data["RadAjaxPanel1PostDataValue"] = ""
    r = session.post(url, data=data)

    page = pq(r.text[r.text.find("<body>"):r.text.find("</body>")])
    for tr in page.find("tr")[1:]:
        fecha = tr.cssselect("td")[3].text
        fecha_array = fecha.split("-")
        fecha = "{}-{}-{}".format(fecha_array[2], fecha_array[1], fecha_array[0])
        if Multa.objects.filter(codigo=tr.cssselect("td")[1].text, empresa=empresa).count() > 1:
            print("duplicadadupl")
            Multa.objects.filter(codigo=tr.cssselect("td")[1].text, empresa=empresa).delete()
        obj, created = Multa.objects.update_or_create(
            codigo=tr.cssselect("td")[1].text,
            empresa=empresa,
            defaults={

                'procedencia': tr.cssselect("td")[0].text,
                'multa': tr.cssselect("td")[1].text,
                'estado': tr.cssselect("td")[2].text,
                'fecha': fecha,
                'monto_um': tr.cssselect("td")[4].text,
                'tipo_um': tr.cssselect("td")[5].text,
                'enunciado': tr.cssselect("td")[6].text,
                'razonsocial': razonsocial,
                'rut': empresa.rut,
                'dv': empresa.dv
            }
        )


def read_nuevas():
    empresas = Empresa.objects.filter(updated_at__isnull=True).order_by('?')[0:400]
    for empresa in empresas:
        print("Leer Rut {}-{}".format(empresa.rut, empresa.dv))
        read_by_empresa(empresa)
        empresa.updated_at = datetime.now()
        empresa.save()


def read_reread():
    updated_at = Empresa.objects.all().order_by('updated_at').first().updated_at
    empresas = Empresa.objects.filter(updated_at=updated_at).order_by('?')[0:240]
    for empresa in empresas:
        print("Leer Rut {}-{}".format(empresa.rut, empresa.dv))
        read_by_empresa(empresa)
        empresa.updated_at = datetime.now()
        empresa.save()


def traspasar_empresas():
    import csv
    with open('final/direccion_trabajo__empresas.csv', 'r') as csvFile:
        k = 0
        total = 0
        empresas = []
        reader = csv.reader(csvFile)
        for row in reader:
            k += 1
            total += 1
            if total % 1000 == 0:
                print(total)
            if row[0] == 'id':
                continue
            empresas.append(Empresa(rut=row[0], dv=row[4]))

            if k == 1000:
                Empresa.objects.bulk_create(empresas)
                k = 0
                empresas = []

        Empresa.objects.bulk_create(empresas)
