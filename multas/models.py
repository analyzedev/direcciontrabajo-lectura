from django.db import models


class Empresa(models.Model):
    """
       Modelo que registra la planificación ordenes de la semana, para su generación masiva.
    """
    rut = models.TextField(null=True, blank=True, db_index=True)
    ruti = models.IntegerField(null=True, blank=True, db_index=True)
    dv = models.TextField(null=True, blank=True)
    razonsocial = models.TextField(null=True, blank=True)
    anio = models.TextField(null=True, blank=True)
    updated_at = models.DateField(null=True, blank=True, db_index=True)
    seleccion = models.BooleanField(default=False, help_text='Ruts que son parte de la muestra de seleccion')
    incremental = models.BooleanField(default=False, db_index=True, help_text='temporal para agregar todos los ruts y saber en cual van')
    incremental2 = models.BooleanField(default=False, db_index=True, help_text='temporal para agregar todos los ruts y saber en cual van')

    class Meta:
        permissions = (

        )

    def save(self, *args, **kwargs):
        self.ruti = self.rut
        super(Empresa, self).save(*args, **kwargs)


class Multa(models.Model):
    codigo = models.TextField(null=True, blank=True)
    empresa = models.ForeignKey(Empresa, on_delete=models.CASCADE)
    procedencia = models.TextField(null=True, blank=True)
    multa = models.TextField(null=True, blank=True)
    estado = models.TextField(null=True, blank=True)
    fecha = models.DateField(null=True, blank=True)
    monto_um = models.TextField(null=True, blank=True)
    tipo_um = models.TextField(null=True, blank=True)
    enunciado = models.TextField(null=True, blank=True)
    rut = models.TextField(null=True, blank=True)
    dv = models.TextField(null=True, blank=True)
    razonsocial = models.TextField(null=True, blank=True)

    class Meta:
        permissions = ()
