# Generated by Django 2.2.5 on 2019-11-05 18:02

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('multas', '0009_auto_20191105_1757'),
    ]

    operations = [
        migrations.AlterField(
            model_name='empresa',
            name='rut',
            field=models.TextField(blank=True, db_index=True, null=True),
        ),
    ]
