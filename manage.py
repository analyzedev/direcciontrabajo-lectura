#!/usr/bin/env python
import os
import sys
import json

if __name__ == "__main__":
    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "conf.settings")
    os.environ["APP_DEBUG"] = "0"
    os.environ["APP_DEBUG_TOOLBAR"] = "0"
    os.environ["APP_DATABASES"] = json.dumps({"default":{"ENGINE":"django.db.backends.postgresql_psycopg2","NAME":"chilecompra","USER":"chilecompra","PASSWORD":"canalyze1020","HOST":"chilecompra.cswc2b3xyxax.us-west-2.rds.amazonaws.com","PORT":"5432"},"sodexo":{"ENGINE":"django.db.backends.postgresql_psycopg2","NAME":"chilecompra","USER":"sodexo","PASSWORD":"sodexo1020","HOST":"sodexo.cswc2b3xyxax.us-west-2.rds.amazonaws.com","PORT":"5432"},"backend":{"ENGINE":"django.db.backends.postgresql_psycopg2","NAME":"chilecompra","USER":"backend","PASSWORD":"canalyze1020","HOST":"backend.cswc2b3xyxax.us-west-2.rds.amazonaws.com","PORT":"5432"},"frontend":{"ENGINE":"django.db.backends.postgresql_psycopg2","NAME":"chilecompra","USER":"backend","PASSWORD":"canalyze1020","HOST":"frontend.cswc2b3xyxax.us-west-2.rds.amazonaws.com","PORT":"5432"},"indicadores":{"ENGINE":"django.db.backends.postgresql_psycopg2","NAME":"chilecompra","USER":"backend","PASSWORD":"canalyze1020","HOST":"backend.cswc2b3xyxax.us-west-2.rds.amazonaws.com","PORT":"5432"},"master":{"ENGINE":"django.db.backends.postgresql_psycopg2","NAME":"chilecompra","USER":"analyzesql","PASSWORD":"canalyzemaster1020","HOST":"chilecompra.cswc2b3xyxax.us-west-2.rds.amazonaws.com","PORT":"5432"},"redshift":{"ENGINE":"django_redshift_backend","NAME":"dev","USER":"analyzeredshift","PASSWORD":"Canalyzemaster1020","HOST":"chilecompra.clfs16zcgcow.us-west-2.redshift.amazonaws.com","PORT":"5439"}})
    os.environ["APP_SENTRY_DSN"] = "https://9d5ce873e8914b25be5aadb01de45566:50bb4398771740199f219bc771e5778c@sentry.io/223751"
    os.environ["APP_DATADOG_API_KEY"] = "2eb67cf8a5ab5f360ecde34cc3af7295"
    os.environ["APP_DATADOG_APP_KEY"] = "1a38a81059a3c14d768b46ef8c16d3793fd07d2c"
    os.environ["APP_EMAIL_HOST_PASSWORD"] = "canalyze1020"
    # os.environ["APP_REDIS_HOST"]        = 'chilecompra.1bswwq.0001.usw2.cache.amazonaws.com'
    os.environ["APP_REDIS_HOST"]        = 'localhost'

    from django.core.management import execute_from_command_line

    execute_from_command_line(sys.argv)

